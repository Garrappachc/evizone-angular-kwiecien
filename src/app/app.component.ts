import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <!-- https://jsfiddle.net/mz gs 0u um/ --> 

    <nav class="navbar navbar-expand navbar-dark bg-dark mb-4">
      <div class="container">
        <a class="navbar-brand" routerLink="/" 
                                routerLinkActive="active"
                                [routerLinkActiveOptions]="{ exact:true }"
                                #rla="routerLinkActive"
                                > Music App </a>
        <div class="collapse navbar-collapse">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" routerLink="/playlists" 
                                  routerLinkActive="active">Playlists</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/music" 
                                  routerLinkActive="active">Search Music</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>


    <div class="container">
      <div class="row">
        <div class="col">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'App';

  hide = false

  constructor() {

  }
}
