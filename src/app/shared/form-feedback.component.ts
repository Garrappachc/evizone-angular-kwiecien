import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form-feedback',
  template: `
  <ng-container *ngIf="field.touched || field.dirty">
    <div *ngIf="field.hasError('required')">Field is required</div>
    <div *ngIf="field.getError('minlength') as error">
      Field has to have minumum {{error.requiredLength}} characters
    </div>
    <ng-content></ng-content>
  </ng-container>
`,
  styles: []
})
export class FormFeedbackComponent implements OnInit {

  @Input()
  field:AbstractControl

  constructor() { }

  ngOnInit() {
  }

}
