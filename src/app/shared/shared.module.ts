import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './highlight.directive';
import { UnlessDirective } from './unless.directive';
import { FormFeedbackComponent } from './form-feedback.component';

@NgModule({
  imports: [
    CommonModule // ngIf, ngSwich, ngFor
  ],
  declarations: [
    HighlightDirective,
    UnlessDirective,
    FormFeedbackComponent
  ],
  exports:[
    HighlightDirective,
    UnlessDirective,
    FormFeedbackComponent
  ]
})
export class SharedModule { }
