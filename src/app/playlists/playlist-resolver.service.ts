import { Injectable } from '@angular/core';
import { Playlist } from '../models/playlist';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PlaylistsService } from './playlists.service';

@Injectable()
export class PlaylistResolverService implements Resolve<Playlist>{

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.service.getPlaylist(parseInt(route.paramMap.get('id')))
  }

  constructor(
    private service: PlaylistsService
  ) { }

}
