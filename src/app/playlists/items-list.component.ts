import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from '../models/playlist';
import { Identity } from '../models/album';

// [style.color]=" hover === playlist? playlist.color : 'initial' "
// (mouseenter)="hover = playlist"
// (mouseleave)="hover = null"

@Component({
  selector: 'app-items-list',
  template: `
    <div class="list-group">
      <div class="list-group-item list-group-item-action"
          (click)="select(playlist)"
          [class.active]="selected?.id === playlist.id"
          [style.borderLeftColor]="playlist.color"
          
          [appHighlight]="playlist.color"

          [routerLink]="['/playlists',playlist.id]"

          *ngFor="let playlist of items; index as i; trackBy identityFn">
        {{i+1}}. {{playlist.name}}
      </div>
    </div>
  `,
  styles: [`
    .list-group-item{
      border-left:10px solid
    }
  `]
})
export class ItemsListComponent<T extends Identity> implements OnInit {

  @Input()
  items: T[]

  @Output()
  selectedChange = new EventEmitter<T>()

  @Input()
  selected:T

  select(playlist:T){  
    this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit() {
  }

  identityFn(item:T){
    return item.id
  }
}

// class ColoredItemsList extends ItemsListComponent<{color:string; id:string}>
