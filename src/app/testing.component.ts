import { Component, OnInit, Inject, InjectionToken } from '@angular/core';

export const TestingServiceToken = new InjectionToken('opis')

@Component({
  selector: 'app-testing',
  template: `
    <p>{{message}}</p>
    <input type="text" 
      [(ngModel)]="message">
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message

  constructor(
    @Inject(TestingServiceToken)
    private service
  ) { 
    this.message = this.service.getMessage()
  }

  ngOnInit() {
  }

}
