import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { Album } from '../models/album';

@Component({
  selector: 'app-album-item',
  template: `
    <img class="card-img-top" 
      [src]="album.images[1]?.url">
    
    <div class="card-body">
      <h5 class="card-title">
        {{album.name}}
      </h5>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input()
  album:Album

  @HostBinding('class.card')
  card = true

  constructor() { }

  ngOnInit() {
  }

}
