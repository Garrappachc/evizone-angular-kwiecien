import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Album } from '../models/album';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
// import 'rxjs/Rx'
// import 'rxjs/add/operator/map'
import { map, catchError, delay, startWith, tap, switchMap, share, shareReplay } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';

import { _throw } from 'rxjs/observable/throw';

export const SEARCH_URL = new InjectionToken<string>('Url for music search service')

enum ResponseCodes {
  NOT_AUTHORIZED = 401
}

interface AlbumsResponse {
  albums: {
    items: Album[]
  }
}


@Injectable()
export class MusicService {

  albums$ = new ReplaySubject<Album[]>(1)
  queries$ = new ReplaySubject<string>(1)

  constructor(
    private http: HttpClient,
    @Inject(SEARCH_URL) private url: string,
  ) {

    this.queries$.pipe(

      map(query => ({
        type: 'album',
        q: query
      })),

      switchMap(params => this.http.get<AlbumsResponse>(this.url, {
        observe:'response',
        params,
      })),

      map( (response:HttpResponse<AlbumsResponse>) => {

        // total = response.headers.get('X-Total')
        // next = response.body.albums.next
        // ....
        
        return response.body.albums.items
      }),
    )
      .subscribe(this.albums$)
      // .subscribe(albums => this.albums$.next(albums))
  }

  search(query = 'batman') {
    this.queries$.next(query)
  }

  clear(){
    this.albums$.next([])
  }


  getAlbums(): Observable<Album[]> {
    return this.albums$.asObservable()
  }
}

// import { Subject } from 'rxjs/Subject'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { ReplaySubject } from 'rxjs/ReplaySubject'
import { of } from 'rxjs/observable/of';