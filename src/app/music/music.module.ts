import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { environment } from '../../environments/environment';
import { MusicService, SEARCH_URL } from './music.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MusicRoutingModule } from './music.routing.module';
import { AuthModule } from '../auth/auth.module';

@NgModule({
  imports: [
    CommonModule,
    AuthModule.forChild(),
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule,
    MusicRoutingModule
  ],
  declarations: [
    MusicSearchComponent, SearchFormComponent, AlbumsListComponent, AlbumItemComponent
  ],
  exports: [
    MusicSearchComponent
  ],
  providers:[
    {
      provide: SEARCH_URL,
      useValue: environment.search_url
    },
    // {
    //   provide: MusicService,
    //   useFactory: (url) => {
    //     return new MusicService(url)
    //   }, 
    //   deps: ['SEARCH_URL']
    // },
    // {
    //   provide: AbstractMusicService,
    //   useClass: SpecialChristmasMusicService
    // },
    MusicService
  ]
})
export class MusicModule { }


// environment.ts:
// search_url: "https://api.spotify.com/v1/search"