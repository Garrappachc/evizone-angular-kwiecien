import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../models/album';

@Component({
  selector: 'app-albums-list',
  template: `    
    <div class="card-group">
      <app-album-item 
      [album]="album"
      *ngFor="let album of albums">
      </app-album-item>
    </div>
  `,
  styles: [`
    app-album-item {
      flex: 0 1 25% !important;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums:Album[]

  constructor() { }

  ngOnInit() {
  }

}
